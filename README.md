# Incometer

A tool allowing a group of people to share their income figures anonymously
and get back aggregated stats to see if they are roughly on the same level
or not.

# Deployment

## Docker deployment

*To be defined - need to figure out whether I want to bake Let's Encrypt SSL certificates
into the image or to fetch them from the mounted volume. See `docker` branch here for this
ongoing work.*

## Apache2 WSGI on a Digital Ocean droplet

This section explains how to deploy the app to a Digital Ocean droplet, which is
noticeably cheaper to run than AWS EC2 (but the instructions are the same with other
Ubuntu machines).

This section is a compilation of Digital Ocean own instructions for
[Flask](https://www.digitalocean.com/community/tutorials/how-to-deploy-a-flask-application-on-an-ubuntu-vps)
and [Django](https://www.digitalocean.com/community/tutorials/how-to-run-django-with-mod_wsgi-and-apache-with-a-virtualenv-python-environment-on-a-debian-vps)
apps as the first one is missing some important steps.

It is assumed in the steps below that the droplet comes with Python 3 (3.8 as of today)
pre-installed as `python3` and that we have root privileges (it's a quick dev
setup anyway, the proper one would be docker-based) - but you don't work as
root, use `sudo` with the commands below.

### Step 1: install prerequisites
* `apt-get update`
* `apt-get install apache2 libapache2-mod-wsgi-py3 python-dev a2enmod python3-pip python3-venv`
* `a3enmod wsgi`

### Step 2: create application user and data folder
* `adduser incometer`
* `mkdir /opt/incometer`
* `chown incometer:incometer /opt/incometer`

### Step 3: clone the app code, create virtual Python environment
* `cd /var/www`
* `git clone https://gitlab.com/yakopov/incometer.git`
* `cd incometer/web`
* `python3 -m venv ./venv`
* `source ./venv/bin/activate`
* `pip install -r requirements.txt`
* `chown -R incometer:incometer /var/www/incometer`

### Step 4: run the tests and init the DB
Switch into app user from root:
* `su incometer`
* `source ./venv/bin/activate`
Run the tests:
* `nose2`
If everything is fine, initialise the database
* `export FLASK_ENV=test`
* `python ./init_db.py`

### Step 5: configure Apache WSGI
* `cp ../setup/digitalocean/incometer.conf /etc/apache2/sites-available/incometer.conf`
* `cp ../setup/digitalocean/incometer.wsgi incometer.wsgi`
* Edit the app secret key and config environment in the the last file if required.

### Step 6: restart Apache to apply changes
Return to root:
* `exit`
Restart Apache
* `service apache2 restart`

This enabled application access in plain HTTP mode - check if it works.
In case app gives an error 500 see `/var/logs/apache2/error.log`

### Step 7: add Let's Encrypt SSL certificate

Follow steps 1-6 of [certbot installation instructions for Ubuntu 20.4](https://certbot.eff.org/lets-encrypt/ubuntufocal-apache)
* `snap install core`
* `sudo snap refresh core`
* `snap install --classic certbot`
* `ln -s /snap/bin/certbot /usr/bin/certbot`

Now if you proceed with step 6, it will fail because certbot is [unable to clone
your WSGI Daemon name](https://stackoverflow.com/a/57000004).
*  Comment out the `WSGIDaemonProcess` line in your `/etc/apache2/sites-available/incometer.conf`

Now run step 7 and the remaining ones:
* `sudo certbot --apache`
* `certbot renew --dry-run`

Fix the daemon names:
* Uncomment out the `WSGIDaemonProcess` line in your `/etc/apache2/sites-available/incometer.conf`
* In `/etc/apache2/sites-available/incometer-le-ssl.conf` created by certbot, change
`WSGIDaemonProcess` and `WSGIProcessGroup` from `incometer` to `incometer-ssl`

### Step 8: restart Apache to apply changes
* `service apache2 restart`

Application should how be accessible via HTTPS. Virtual host for plain HTTP version
can be removed later.