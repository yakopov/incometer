#!/var/www/incometer/web/venv/bin/python
import os
import sys

sys.path.insert(0, '/var/www/incometer/web')
os.environ['FLASK_ENV'] = 'test'

from flask_app import app as application

application.secret_key = 'Replace me later please'