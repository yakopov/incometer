import logging

from flask_app import app


logger = logging.getLogger("Incometer")
logger.setLevel(app.config.get('LOGGER_LEVEL'))

log_formatter = logging.Formatter("%(asctime)s [%(levelname)-5.5s]  %(message)s")

# enable printing into STDOUT
console_handler = logging.StreamHandler()
console_handler.setFormatter(log_formatter)
logger.addHandler(console_handler)