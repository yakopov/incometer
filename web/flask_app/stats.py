import math
import pandas as pd
from io import BytesIO

import matplotlib
matplotlib.use('agg')  # define AGG non-interactive backend, specifically for use on MacOSX
import matplotlib.pyplot as plt
plt.ioff()
from matplotlib.ticker import MaxNLocator

from flask_app import utils


def apply_banding(vals: list, band_size: int) -> list:
    banded_vals = []

    for v in vals:
        band = math.ceil(v / band_size) * band_size
        banded_vals.append(int(band))

    return banded_vals


def get_bin_count(min_val: int, max_val: int, band_size: int) -> int:
    if band_size == 0:
        raise ValueError("Band size cannot be zero")

    if max_val < min_val:
        raise ValueError(
            "Max value submitted for banding ({}) is not expected to be lower than the min one ({})".format(
                max_val, min_val
            ))

    bin_count = math.ceil((max_val - min_val) / band_size)

    if bin_count == 0:
        return 1

    return bin_count


def build_histogram(vals: pd.Series, band_size: int) -> str:
    fig = plt.figure(figsize=(4, 4), tight_layout=True)

    bin_count = get_bin_count(vals.min(), vals.max(), band_size)

    plt.hist(vals, bins=bin_count, facecolor='blue', alpha=0.5)
    plt.grid()
    plt.xlabel("Reported figures banded by {}".format(band_size))
    plt.ylabel("Answers in band")
    plt.title("Distribution of {} figures".format(len(vals)))

    ax = fig.gca()
    ax.yaxis.set_major_locator(MaxNLocator(integer=True))

    img_stream = BytesIO()
    plt.savefig(img_stream, format='png', bbox_inches='tight')

    return utils.encode_bytes_base64(img_stream)
