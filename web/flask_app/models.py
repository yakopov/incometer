from sqlalchemy import event
from sqlalchemy.engine import Engine
from sqlite3 import Connection as SQLite3Connection

from flask_app import app, db

_SAMPLE_ISO_DATE_STR = '2020-12-12T12:12:00'
_DATE_FIELD_LEN = len(_SAMPLE_ISO_DATE_STR)
_SHA512_HEX_LEN = 128
_SALT_LEN = 64
_EMAIL_ADDRESS_LEN = 254
_UUID_LEN = 32


@event.listens_for(Engine, "connect")
def _set_sqlite_pragma(dbapi_connection, connection_record):
    """
    Enables cascade deletions in SQLite as per https://stackoverflow.com/a/62327279
    """
    if isinstance(dbapi_connection, SQLite3Connection):
        cursor = dbapi_connection.cursor()
        cursor.execute("PRAGMA foreign_keys=ON;")
        cursor.close()


# at the moment this table only holds admin users
# it is unclear if persistent user profiles for surveys would be needed
# as of now I would prefer to avoid that
class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(_EMAIL_ADDRESS_LEN), unique=True, nullable=False)

    password_salt = db.Column(db.String(64), unique=False, nullable=False)
    password_hash = db.Column(db.String(_SHA512_HEX_LEN), unique=False, nullable=False)

    def __repr__(self):
        return "<User {} {}>".format(self.id, self.email)


class Survey(db.Model):
    id = db.Column(db.Integer, primary_key=True)

    title = db.Column(db.String(256), unique=False, nullable=True)
    description = db.Column(db.String(1024), unique=False, nullable=True)

    # key to share with people who will be submitting their answers
    user_key = db.Column(db.String(_UUID_LEN), unique=True, nullable=False, index=True)

    # key to be used as password to access the survey, e.g. to delete it
    admin_key_salt = db.Column(db.String(_SALT_LEN), unique=True, nullable=False)
    admin_key_hash = db.Column(db.String(_SHA512_HEX_LEN), unique=True, nullable=False)

    reveal_after = db.Column(db.Integer, unique=False, nullable=True)
    band_size = db.Column(db.Integer, unique=False, nullable=True)

    datetime_utc = db.Column(db.String(_DATE_FIELD_LEN), unique=False, nullable=False)

    is_locked = db.Column(db.Integer, unique=False, nullable=False, default=0)

    answers = db.relationship('Answer', backref='answers', passive_deletes=True)

    def __repr__(self):
        return "<Survey {} of {}>".format(self.id, self.datetime_utc)


class Answer(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    survey_id = db.Column(db.Integer, db.ForeignKey(Survey.id, ondelete='CASCADE'),
                          unique=False, nullable=False, index=True)

    value = db.Column(db.Integer, unique=False, nullable=False)

    # trackable user information to make repeated submission a bit more difficult
    fingerprint_salt = db.Column(db.String(_SALT_LEN), unique=True, nullable=False)
    fingerprint_hash = db.Column(db.String(_SHA512_HEX_LEN), unique=False, nullable=False)

    datetime_utc = db.Column(db.String(_DATE_FIELD_LEN), unique=False, nullable=False)

    # survey = db.relationship('Survey', foreign_keys='Answer.survey_id')

    def __repr__(self):
        return "<Answer {} of {} for survey {}>".format(self.id, self.datetime_utc, self.survey_id)
