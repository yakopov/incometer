import uuid
from sqlalchemy.exc import IntegrityError
from sqlalchemy import func

from flask_app import app, db, models, utils
from flask_app.logger import logger


def create_survey(title: str, description: str, reveal_after: int = None, band_size: int = None) -> tuple:
    if (reveal_after is not None) and (reveal_after < 1):
        raise ValueError("Number of submissions should be 1 or more, if specified")

    if (band_size is not None) and (band_size < 1):
        raise ValueError("Band size should be 1 or more, if specified")

    if (title is not None) and (len(title) > models.Survey.title.property.columns[0].type.length):
        raise ValueError("Title is too long")

    if (description is not None) and (len(description) > models.Survey.description.property.columns[0].type.length):
        raise ValueError("Description is too long")

    admin_key = uuid.uuid4().hex
    admin_key_salt, admin_key_hash = utils.salt_password(admin_key)

    user_key = uuid.uuid4().hex

    survey = models.Survey(title=title, description=description,
                           admin_key_salt=admin_key_salt, admin_key_hash=admin_key_hash,
                           user_key=user_key,
                           reveal_after=reveal_after, band_size=band_size,
                           datetime_utc=utils.get_utc_datetime_str())

    try:
        db.session.add(survey)
        db.session.commit()
        logger.debug("Created survey {}, user key: {}, admin key: {}".format(survey.id, user_key, admin_key))
    except IntegrityError as er:
        logger.warning("Failed to create a survey, perhaps exceptionally rare key collision?")
        raise er

    return survey, admin_key


def get_survey(user_key: str) -> models.Survey:
    return models.Survey.query.filter_by(user_key=user_key).first()


def get_survey_answers(survey_id: int) -> list:
    answers = models.Answer.query.filter_by(survey_id=survey_id).all()

    return answers


def _get_count(q) -> int:
    """
    Implements fast count query in SQLAlchemy as the default method would create a SELECT * subquery

    https://gist.github.com/hest/8798884
    """
    count_q = q.statement.with_only_columns([func.count()]).order_by(None)
    count = q.session.execute(count_q).scalar()
    return count


def get_survey_answer_count(survey_id: int) -> int:
    query = db.session.query(models.Answer).filter_by(survey_id=survey_id)
    return _get_count(query)


def add_answer(user_key: str, value: int, fingerprint: str) -> models.Answer:
    survey = get_survey(user_key)
    if not survey:
        raise ValueError("Survey {} not found".format(user_key))

    if app.config['MAX_ANSWERS_PER_SURVEY'] <= get_survey_answer_count(survey.id):
        raise ValueError("Maximal number of answers of {} is reached or exceeded for this query".format(
            app.config['MAX_ANSWERS_PER_SURVEY']
        ))

    if not(int(value) > 0):
        raise ValueError("Submitted figure must be above zero")

    fingerprint_salt, fingerprint_hash = utils.salt_password(fingerprint)

    # TODO: add a check for an existing fingerprint - right now it'll complicate debugging

    answer = models.Answer(survey_id=survey.id, value=value,
                           fingerprint_salt=fingerprint_salt, fingerprint_hash=fingerprint_hash,
                           datetime_utc=utils.get_utc_datetime_str())

    db.session.add(answer)
    db.session.commit()

    return answer


def set_survey_locked(survey: models.Survey, is_locked: bool) -> models.Survey:
    if (survey.is_locked == 1) == is_locked:
        return survey

    survey.is_locked = 1 if is_locked else 0
    db.session.commit()

    return survey


def delete_survey(user_key: str) -> bool:
    survey = get_survey(user_key)
    if not survey:
        return False

    db.session.delete(survey)
    # db.session.query(models.Survey).filter_by(id=survey.id).delete()
    db.session.commit()

    return True
