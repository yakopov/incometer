from flask import session, request, flash, redirect
from sqlalchemy.sql import func

from flask_app import app, models, db
from flask_app.utils import render, recreate_password_hash


def _admin_render(template_path: str, params: dict = None) -> str:
    if params is None:
        params = {}

    params['logged_in'] = session.get('logged_in')

    return render(template_path, params)


@app.route('/admin/dashboard')
def admin_dashboard():
    if not session.get('logged_in'):
        return redirect('/admin')

    survey_count = models.Survey.query.count()
    survey_oldest = db.session.query(func.min(models.Survey.datetime_utc).label('min')).first()
    survey_latest = db.session.query(func.max(models.Survey.datetime_utc).label('max')).first()

    return _admin_render('admin/dashboard.html', {
        'survey_count': survey_count,
        'survey_oldest': 'None' if survey_oldest.min is None else survey_oldest.min,
        'survey_latest': 'None' if survey_latest.max is None else survey_latest.max,
        'config': app.config
    })


@app.route('/admin/profile')
def admin_profile():
    if not session.get('logged_in'):
        return redirect('/admin')

    return _admin_render('admin/profile.html')


@app.route('/admin', methods=['GET', 'POST'])
def do_admin_login():
    if request.method == 'POST':
        admin = models.User.query.filter_by(email=request.form['email']).first()
        if admin:
            admin_password_hash = recreate_password_hash(admin.password_salt, request.form['password'])

            if admin_password_hash == admin.password_hash:
                session['logged_in'] = True
                return redirect('/admin/dashboard')

        flash("Wrong credentials!")

    return _admin_render('/admin/login.html')


@app.route("/admin/logout")
def do_admin_logout():
    session['logged_in'] = False
    return redirect('/admin')
