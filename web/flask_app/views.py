from flask import request, redirect, flash
from sqlalchemy.exc import IntegrityError
import pandas as pd

from flask_app import app, surveys, utils, models, stats
from flask_app.logger import logger


@app.route('/')
def index() -> str:
    return utils.render('public/index.html')


@app.route('/about')
def about() -> str:
    return utils.render('public/about.html')


@app.route('/survey/add', methods=['GET', 'POST'])
def add_survey() -> str:
    if request.method == 'POST':
        try:
            reveal_after = utils.parse_int_param(request.form['reveal_after'])
            band_size = utils.parse_int_param(request.form['band_size'])
        except ValueError:
            logger.debug("Failed to parse integers")
            flash("Integer or no value expected")
            return utils.render('/public/survey_add.html')

        try:
            survey, admin_key = surveys.create_survey(
                request.form['title'], request.form['description'],
                reveal_after, band_size
            )
            logger.debug("Created survey {}, admin_key {}".format(survey, admin_key))
        except ValueError as er:
            logger.debug(str(er))
            flash(str(er))
            return utils.render('/public/survey_add.html')

        except IntegrityError as er:
            logger.warning(str(er))
            flash(str(er))
            return utils.render('/public/survey_add.html')

        return utils.render('/public/survey_added.html', {'user_key': survey.user_key, 'admin_key': admin_key})

    return utils.render('/public/survey_add.html')


@app.route('/survey/added')
def survey_added(user_key: str = None, admin_key: str = None) -> str:
    if (user_key is None) or (admin_key is None):
        flash("Invalid request")
        return redirect('/survey/add')

    return utils.render('/public/survey_added.html', {'user_key': user_key, 'admin_key': admin_key})


@app.route('/survey/answer', methods=['GET', 'POST'])
def add_answer() -> str:
    if request.method == 'POST':
        user_key = request.form['user_key']
        post_form_params = {'user_key': user_key}

        survey = surveys.get_survey(user_key)
        if not survey:
            return utils.render('/public/survey_not_found.html', post_form_params)

        try:
            value = utils.parse_int_param(request.form['answer'], False)
        except ValueError:
            flash("Integer value above 0 expected")
            return utils.render('/public/answer_add.html', post_form_params)

        try:
            # TODO: use more nuanced fingerprint than just IP
            answer = surveys.add_answer(request.form['user_key'], value, request.remote_addr)
            logger.debug("Created answer {}".format(answer))
        except ValueError as er:
            logger.debug(str(er))
            flash(str(er))
            return utils.render('/public/answer_add.html', post_form_params)

        return utils.render('/public/answer_added.html', {'user_key': user_key})

    user_key = request.args.get('user_key')
    survey = surveys.get_survey(user_key)
    get_form_params = {'user_key': user_key}

    if survey and not survey.is_locked:
        return utils.render('/public/answer_add.html', {
            'survey_title': survey.title,
            'survey_description': survey.description,
            'survey_band_size': survey.band_size,

            'user_key': survey.user_key
        })

    return utils.render('/public/survey_not_found.html', get_form_params)


@app.route('/survey/results')
def survey_results() -> str:
    user_key = request.args.get('user_key')
    survey = surveys.get_survey(user_key)

    if not survey:
        return utils.render('/public/survey_not_found.html', {'user_key': user_key})

    """
    if survey.is_locked:
        return utils.render('/public/survey_not_found.html', {'user_key': user_key})
    """

    answer_count = surveys.get_survey_answer_count(survey.id)

    if survey.is_locked or (answer_count < survey.reveal_after):
        return utils.render('/public/survey_not_ready.html', {
            'survey_title': survey.title,
            'survey_description': survey.description,
            'survey_band_size': survey.band_size,
            'survey_reveal_after': survey.reveal_after,
            'survey_remain': survey.reveal_after - answer_count,
            'survey_datetime_utc': survey.datetime_utc,
            'user_key': survey.user_key,
        })

    answers = surveys.get_survey_answers(survey.id)
    values = [row.value for row in answers]
    values = stats.apply_banding(values, survey.band_size)
    df = pd.DataFrame({'values': values})

    return utils.render('/public/survey_results.html', {
        'survey_title': survey.title,
        'survey_description': survey.description,
        'survey_band_size': survey.band_size,
        'survey_reveal_after': survey.reveal_after,
        'survey_datetime_utc': survey.datetime_utc,
        'user_key': survey.user_key,

        'result_count': df.shape[0],
        'result_median': df['values'].median(),
        'result_mean': df['values'].mean(),
        'result_min_max_diff': 100 * df['values'].max() / df['values'].min(),

        'result_histogram': stats.build_histogram(df['values'], survey.band_size)
    })


@app.route('/survey/update', methods=['GET', 'POST'])
def survey_update() -> str:
    user_key = None
    admin_key = None

    if request.method == 'POST':
        user_key = request.form['user_key']
        post_form_params = {'user_key': user_key}

        survey = surveys.get_survey(user_key)
        if not survey:
            return utils.render('/public/survey_not_found.html', post_form_params)

        admin_key = request.form['admin_key']
        admin_hash = utils.recreate_password_hash(survey.admin_key_salt, admin_key)
        if admin_hash != survey.admin_key_hash:
            return utils.render('/public/survey_not_found.html', post_form_params)

        if ('delete' in request.form) and (request.form['delete'] == 'True'):
            surveys.delete_survey(survey.user_key)
            flash("Survey {} was deleted along with any answers submitted".format(user_key))
            return utils.render('/public/survey_not_found.html', post_form_params)

        if 'lock' in request.form:
            new_lock_status = not(request.form['lock'] == '1')
            survey = surveys.set_survey_locked(survey, new_lock_status)

            flash("Survey has been {}".format("locked" if survey.is_locked else "unlocked"))

    if user_key is None:
        user_key = request.args.get('user_key')

    survey = surveys.get_survey(user_key)
    get_form_params = {'user_key': user_key}

    if not survey:
        return utils.render('/public/survey_not_found.html', get_form_params)

    if admin_key is None:
        admin_key = request.args.get('admin_key')

    admin_hash = utils.recreate_password_hash(survey.admin_key_salt, admin_key)
    if admin_hash != survey.admin_key_hash:
        return utils.render('/public/survey_not_found.html', get_form_params)

    return utils.render('/public/survey_update.html', {
        'survey_title': survey.title,
        'survey_description': survey.description,
        'survey_band_size': survey.band_size,
        'survey_reveal_after': survey.reveal_after,
        'survey_datetime_utc': survey.datetime_utc,
        'survey_is_locked': survey.is_locked,

        'answer_count': surveys.get_survey_answer_count(survey.user_key),

        'user_key': survey.user_key,
        'admin_key': admin_key
    })
