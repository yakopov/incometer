from flask import Flask
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)

if 'ENV' in app.config and (app.config['ENV'] == 'production'):
    app.config.from_object('config.ProductionConfig')
elif 'ENV' in app.config and (app.config['ENV'] == 'test'):
    app.config.from_object('config.TestConfig')
else:
    app.config.from_object('config.DevelopmentConfig')

db = SQLAlchemy(app)

from flask_app import views
from flask_app import admin_views
