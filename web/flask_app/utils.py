from flask import render_template
import random
import string
import hashlib
import typing
import re
from datetime import datetime
from dateutil import tz
import base64
from io import BytesIO

from flask_app import app, models


def render(template_path: str, params: dict = None) -> str:
    if params is None:
        params = {}

    default_params = {
        'mode_caption': app.config['MODE_CAPTION']
    }

    for key, value in default_params.items():
        if key not in params:
            params[key] = value

    return render_template(template_path, **params)


# TODO: could probably be replaced with uuid.uuid4().hex
def get_random_alphanumeric_string(length: int) -> str:
    characters = string.ascii_letters + ''.join([str(num) for num in range(10)])

    return ''.join(random.choice(characters) for i in range(length))


def salt_password(password: str) -> tuple:
    """
    Salts given password string with a random salt one plus static salt
    defined in config, and returns both raw random salt and SHA-512 hash of the salted value
    """
    # automatically adjust the length of the salt string to the length of the
    # corresponding DB field
    salt_length = models.User.password_salt.property.columns[0].type.length
    salt = get_random_alphanumeric_string(salt_length)

    return salt, recreate_password_hash(salt, password)


def recreate_password_hash(salt: str, password: str) -> str:
    salted_password = salt + app.config.get('SALT_SECRET') + password
    return hashlib.sha512(str(salted_password).encode('utf-8')).hexdigest()


def get_utc_datetime_str(dt: datetime = None) -> str:
    if dt is None:
        dt = datetime.now()

    utc_tz = tz.gettz('UTC')
    local_tz = tz.tzlocal()

    # check if the given datetime is naive or timezone-aware
    if (dt.tzinfo is None) or (dt.tzinfo.utcoffset(dt) is None):
        # assign local timezone
        dt = dt.astimezone(local_tz)

    dt = dt.astimezone(utc_tz)
    # make date naive again so it is not reflected in the string representation
    # also remove microseconds for readability
    dt = dt.replace(tzinfo=None, microsecond=0)
    return dt.isoformat()


def parse_int_param(val: str, allow_empty=True) -> typing.Union[int, None]:
    if len(val) == 0:
        if allow_empty:
            return None
        else:
            raise ValueError("Empty value not allowed, integer expected")
    try:
        return int(val)
    except ValueError:
        raise ValueError("Can't accept {} value, integer expected".format(val))


def is_email_address_valid(email_address: str) -> bool:
    # source: https://emailregex.com
    if re.match('(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)', email_address) is None:
        return False

    return True


def encode_bytes_base64(img_stream: BytesIO) -> str:
    img_stream.seek(0)
    return base64.b64encode(img_stream.getvalue()).decode('utf-8')
