import os
import unittest

from flask_app import app, db

TEST_DB = '../../../tmp/test_views.db'


class TestViews(unittest.TestCase):
    def setUp(self):
        app.config['TESTING'] = True
        app.config['WTF_CSRF_ENABLED'] = False
        app.config['DEBUG'] = False

        self.base_path = os.path.dirname(os.path.realpath(__file__))
        app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + os.path.join(self.base_path, TEST_DB)

        self.app = app.test_client()

        db.drop_all()
        db.create_all()

    def tearDown(self):
        if self.base_path:
            os.remove(os.path.join(self.base_path, TEST_DB))

    def test_get_survey(self):
        response = self.app.get('/', follow_redirects=True)
        self.assertEqual(response.status_code, 200)

    def test_static(self):
        response = self.app.get('/static/css/style.css')
        self.assertEqual(response.status_code, 200)


if __name__ == "__main__":
    unittest.main()
