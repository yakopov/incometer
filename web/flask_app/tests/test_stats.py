import unittest
from unittest.mock import patch
import pandas as pd

from flask_app import app, stats


class TestStats(unittest.TestCase):
    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_apply_banding(self):
        values = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

        result = stats.apply_banding(values, 1)
        self.assertListEqual(result, values)

        result = stats.apply_banding(values, 100)
        self.assertListEqual(result, [100] * len(result))

        result = stats.apply_banding(values, 4)
        self.assertListEqual(result, [4, 4, 4, 4, 8, 8, 8, 8, 12, 12])

    def test_get_bin_count(self):
        with self.assertRaises(ValueError):
            stats.get_bin_count(10, 100, 0)

        with self.assertRaises(ValueError):
            stats.get_bin_count(100, 10, 0)

        self.assertEqual(stats.get_bin_count(10, 10, 100), 1)
        self.assertEqual(stats.get_bin_count(10, 10, 10), 1)
        self.assertEqual(stats.get_bin_count(10, 10, 9), 1)
        self.assertEqual(stats.get_bin_count(10, 10, 1), 1)

        self.assertEqual(stats.get_bin_count(10, 100, 100), 1)
        self.assertEqual(stats.get_bin_count(10, 100, 1), 90)
        self.assertEqual(stats.get_bin_count(1, 100, 9), 11)

    def test_build_histogram(self):
        # loose testing here, just make sure matplotlib doesn't fail as it sometimes does with wrong settings
        result = stats.build_histogram(pd.Series([10, 20, 30]), 10)

        self.assertTrue(isinstance(result, str))
        self.assertGreater(len(result), 0)


if __name__ == '__main__':
    unittest.main()
