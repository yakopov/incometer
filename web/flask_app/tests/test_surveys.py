import os
import unittest

from flask_app import app, db, surveys, models, utils

TEST_DB = '../../../tmp/test_surveys.db'


class TestSurveys(unittest.TestCase):
    def setUp(self):
        app.config['TESTING'] = True
        app.config['WTF_CSRF_ENABLED'] = False
        app.config['DEBUG'] = False

        app.config['MAX_ANSWERS_PER_SURVEY'] = 1000

        self.base_path = os.path.dirname(os.path.realpath(__file__))
        app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + os.path.join(self.base_path, TEST_DB)

        self.app = app.test_client()

        db.drop_all()
        db.create_all()

    def tearDown(self):
        """
        if self.base_path:
            os.remove(os.path.join(self.base_path, TEST_DB))
        """
        pass

    def test_get_survey(self):
        result = surveys.get_survey('foobar')
        self.assertIsNone(result)

        survey, _ = surveys.create_survey('foo', 'bar', 3, 1000)
        self.assertEqual(survey.id, 1)

        result = surveys.get_survey(survey.user_key)
        self.assertEqual(survey.id, result.id)

    def test_create_survey(self):
        now = utils.get_utc_datetime_str()
        survey, admin_key = surveys.create_survey('foo', 'bar', 3, 1000)
        self.assertEqual(survey.id, 1)
        self.assertGreaterEqual(survey.datetime_utc, now)
        self.assertEqual(survey.admin_key_hash, utils.recreate_password_hash(survey.admin_key_salt, admin_key))

        too_long_title = ''.join([
            'a' for i in range(models.Survey.title.property.columns[0].type.length)]) + 'a'
        with self.assertRaises(ValueError):
            surveys.create_survey(too_long_title, None, None, None)

        too_long_description = ''.join([
            'a' for i in range(models.Survey.description.property.columns[0].type.length)
        ]) + 'a'
        with self.assertRaises(ValueError):
            surveys.create_survey('foo', too_long_description, None, None)

        with self.assertRaises(ValueError):
            surveys.create_survey('foo', 'bar', 0, None)

        with self.assertRaises(ValueError):
            surveys.create_survey('foo', 'bar', -2, None)

        with self.assertRaises(ValueError):
            surveys.create_survey('foo', 'bar', None, 0)

        with self.assertRaises(ValueError):
            surveys.create_survey('foo', 'bar', None, -2)

    def test_get_survey_answers(self):
        result = surveys.get_survey_answers('foobar')
        self.assertListEqual(result, [])

        survey, _ = surveys.create_survey('foo', 'bar', 3, 1000)
        result = surveys.get_survey_answers(survey.id)
        self.assertListEqual(result, [])

        answer1 = surveys.add_answer(survey.user_key, 20000, '127.0.0.1')
        answer2 = surveys.add_answer(survey.user_key, 25000, '127.0.0.1')

        result = surveys.get_survey_answers(survey.id)
        self.assertEqual(len(result), 2)
        self.assertEqual(result[0].id, answer1.id)
        self.assertEqual(result[1].id, answer2.id)

    def test_get_survey_answer_count(self):
        self.assertEqual(surveys.get_survey_answer_count(100), 0)

        survey, _ = surveys.create_survey('foo', 'bar', 3, 1000)
        self.assertEqual(surveys.get_survey_answer_count(survey.id), 0)

        surveys.add_answer(survey.user_key, 20000, '127.0.0.1')
        self.assertEqual(surveys.get_survey_answer_count(survey.id), 1)

        surveys.add_answer(survey.user_key, 25000, '127.0.0.1')
        self.assertEqual(surveys.get_survey_answer_count(survey.id), 2)

        survey2, _ = surveys.create_survey('foo', 'bar', 3, 1000)
        self.assertEqual(surveys.get_survey_answer_count(survey2.id), 0)
        surveys.add_answer(survey2.user_key, 20000, '127.0.0.1')

        self.assertEqual(surveys.get_survey_answer_count(survey.id), 2)

    def test_add_answer(self):
        survey, _ = surveys.create_survey('foo', 'bar', 3, 1000)

        now = utils.get_utc_datetime_str()
        answer = surveys.add_answer(survey.user_key, 20000, '127.0.0.1')
        self.assertEqual(answer.id, 1)
        self.assertGreaterEqual(answer.datetime_utc, now)

        with self.assertRaises(ValueError):
            surveys.add_answer('foobar', 20000, '127.0.0.1')

        with self.assertRaises(ValueError):
            surveys.add_answer(survey.user_key, 0, '127.0.0.1')

        with self.assertRaises(ValueError):
            surveys.add_answer(survey.user_key, -1, '127.0.0.1')

        with self.assertRaises(ValueError):
            surveys.add_answer(survey.user_key, 'foo', '127.0.0.1')

        # TODO: when fingerprint support is added, add a test for it as well

        app.config['MAX_ANSWERS_PER_SURVEY'] = 1
        with self.assertRaises(ValueError):
            surveys.add_answer(survey.user_key, 30000, '127.0.0.1')

    def test_set_survey_locked(self):
        survey, _ = surveys.create_survey('foo', 'bar', 3, 1000)
        self.assertEqual(survey.is_locked, 0)

        result = surveys.set_survey_locked(survey, False)
        self.assertEqual(result.is_locked, survey.is_locked)
        survey = surveys.get_survey(survey.user_key)
        self.assertEqual(survey.is_locked, 0)

        result = surveys.set_survey_locked(survey, True)
        self.assertEqual(result.is_locked, 1)
        survey = surveys.get_survey(survey.user_key)
        self.assertEqual(survey.is_locked, 1)

    def test_delete_survey(self):
        survey1, _ = surveys.create_survey('foo', 'bar', 3, 1000)
        survey2, _ = surveys.create_survey('foo', 'bar', 3, 1000)

        surveys.add_answer(survey1.user_key, 20000, '127.0.0.1')
        surveys.add_answer(survey2.user_key, 20000, '127.0.0.1')

        self.assertFalse(surveys.delete_survey('foobar'))
        self.assertIsNotNone(surveys.get_survey(survey1.user_key))
        self.assertIsNotNone(surveys.get_survey(survey2.user_key))

        self.assertTrue(surveys.delete_survey(survey1.user_key))

        self.assertIsNone(surveys.get_survey(survey1.user_key))
        self.assertEqual(surveys.get_survey_answer_count(survey1.id), 0)

        self.assertIsNotNone(surveys.get_survey(survey2.user_key))
        self.assertEqual(surveys.get_survey_answer_count(survey2.id), 1)


if __name__ == "__main__":
    unittest.main()
