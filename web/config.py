import logging


class Config(object):
    DEBUG = False
    TESTING = False

    SQLALCHEMY_DATABASE_URI = 'sqlite:////opt/incometer/production.db'
    SQLALCHEMY_TRACK_MODIFICATIONS = False

    SALT_SECRET = 'secret'

    PORT = 5000

    SESSION_COOKIE_SECURE = True

    MODE_CAPTION = None         # visible string that would appear in the UI to indicate the environment in use
    MODE_ID = 'production'      # internal string to distinguish between environment

    LOGGER_LEVEL = logging.INFO

    MAX_ANSWERS_PER_SURVEY = 1000


class ProductionConfig(Config):
    pass


class TestConfig(Config):
    TESTING = True

    SQLALCHEMY_DATABASE_URI = 'sqlite:////opt/incometer/test.db'

    MODE_CAPTION = "TEST"
    MODE_ID = 'test'

    LOGGER_LEVEL = logging.DEBUG


class DevelopmentConfig(Config):
    # running locally using built-in WSGI
    DEBUG = True

    SQLALCHEMY_DATABASE_URI = 'sqlite:////tmp/test.db'

    SESSION_COOKIE_SECURE = False

    MODE_CAPTION = "DEV"
    MODE_ID = 'development'

    LOGGER_LEVEL = logging.DEBUG

