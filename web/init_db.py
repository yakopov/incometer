from sqlalchemy.exc import IntegrityError

from flask_app import app, db, models, utils
from flask_app.logger import logger


logger.info("Initialising {} database at {}".format(
    app.config.get('MODE_ID'),
    app.config.get('SQLALCHEMY_DATABASE_URI')))

# initialise database schema
db.create_all()
logger.info("Initialised DB schema")

# create default database content
admin_email = 'yakopov@protonmail.ch'
admin_password = utils.get_random_alphanumeric_string(10)
admin_salt, admin_hash = utils.salt_password(admin_password)
admin = models.User(email=admin_email,
                    password_salt=admin_salt,
                    password_hash=admin_hash)


try:
    db.session.add(admin)
    db.session.commit()
    logger.debug("Created admin user {} with password: {}".format(admin_email, admin_password))
except IntegrityError:
    logger.warning("Admin user {} already exists, skipped".format(admin_email))
