from flask_app import app
from flask_app.logger import logger

import os

if __name__ == '__main__':
    logger.info("Starting...")

    if app.secret_key is None:
        logger.debug("Secret key not specified, generating a random one")
        app.secret_key = os.urandom(12)

    app.run(host='0.0.0.0', port=app.config['PORT'])
